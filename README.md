Hexchat plugin for 'dsc' replacement with real nick from Discord in Newbie Contest network.

Tab autocompletion works too but starts with an empty users list.


Ex:

`[17:12]  dsc|\<user\>: Hello world!`

becomes,

`[17:12] user|Hello world!`
