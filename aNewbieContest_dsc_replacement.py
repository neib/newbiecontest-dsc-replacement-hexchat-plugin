# -*- coding: UTF-8 -*-

import hexchat
import time
import json

__module_name__ = "dsc_replacement"
__module_version__ = "1.1"
__module_description__ = "Nick replacement for the bot 'dsc' with real nick from Discord."


#------------------------------------------------------------------------------#
# ------------------ Nick replacement function. -------------------------------#
def dsc_replace(word, word_eol, userdata):

    ctx = hexchat.get_context()
    if ctx.get_info("channel") == "#newbiecontest" and ctx.get_info("network") == "NewbieContest":

        global user_list
        global user_dict

        #Who is speaking.
        content = word_eol[0]
        enduser = word_eol[0].find('!')
        user = content[1:enduser]

        #Bot message identification.
        if user == "dsc":
            startnick = word_eol[0].find('<')
            endnick = word_eol[0].find('>')
            nick = content[startnick+1:endnick]
            message = content[endnick+2:]

            #Print message.
            ctx.emit_print("Channel Message", nick, message)

            #Update auto completion list.
            if len(user_dict) >= 100:
                user_dict.popitem()[1]
            t = time.strftime("%D:%H:%M:%S")
            user_dict[nick] = t

        else:
            #Print message.
            message = word_eol[3]
            message = message[1:]
            ctx.emit_print("Channel Message", user, message)

            #Update auto completion list.
            if len(user_dict) >= 100:
                user_dict.pop()
            t = time.strftime("%D:%H:%M:%S")
            user_dict[user] = t


        #Convert the dictionary to a list for autocompletion.
        user_list = sorted(user_dict.keys(), key=user_dict.get, reverse=True)
        user_list_encode = [x.encode("utf-8").hex() for x in user_list]
        hexchat.set_pluginpref("user_list",json.dumps(user_list_encode))

        return hexchat.EAT_HEXCHAT

    return hexchat.EAT_NONE



#------------------------------------------------------------------------------#
# ------------------ Auto completion function. --------------------------------#
def add_user(word, word_eol, userdata):

    ctx = hexchat.get_context()
    if ctx.get_info("channel") == "#newbiecontest" and ctx.get_info("network") == "NewbieContest":

        global multiTab
        global tab
        global startcompletion
        global lastWord
        global lastWordIndex
        global user_list
        global user_dict

        #Reset Tab counter if anything else is pressed.
        if word[0] != "65289":
            multiTab = 0
            return

        #Init autocompletion.
        if multiTab == 0:
            startcompletion = ctx.get_info("inputbox")
            lastWordIndex = startcompletion.rfind(' ') + 1
            lastWord = startcompletion[lastWordIndex:]

        #Find all nicks prefixed with lastWord.
        prefixedUsers = [x for x in user_list if x.startswith(lastWord)]
        Users = [x for x in user_list]

        #Cycle multiTab if exceed prefixedUsers[].
        if multiTab > len(prefixedUsers) - 1:
            multiTab = 0

        #Choose the indexed user by multiTab
        autoCompUser = prefixedUsers[multiTab]
        multiTab += 1

        #Autocomplete
        if startcompletion.rfind(' ') != -1:
            completion = startcompletion[:lastWordIndex] + autoCompUser
        else:
            completion = autoCompUser


        cursor = len(completion)
        cmd_completion = "settext " + completion
        cmd_cursor = "setcursor " + str(cursor)
        ctx.command(cmd_completion)
        ctx.command(cmd_cursor)

        return hexchat.EAT_HEXCHAT

    return hexchat.EAT_NONE



#------------------------------------------------------------------------------#
# ------------------ Main. ----------------------------------------------------#
#Take back users list.
user_dict = {}
try:
    user_list = hexchat.get_pluginpref("user_list")
    user_list = user_list.replace('"','').replace(',','').replace('[','').replace(']','')
    user_list = user_list.split(' ')
    user_list = [bytearray.fromhex(x).decode() for x in user_list]
except:
    user_list = [""]

for i in range(0, len(user_list)):
    user_dict[user_list[i]] = time.strftime("%D:%H:%M:%S")

#Init variables of the Tab function
multiTab = 0
tab = 0
startcompletion = ""
lastWord = ""
lastWordIndex = 0


hexchat.hook_server("PRIVMSG", dsc_replace)
hexchat.hook_print("Key Press", add_user)
